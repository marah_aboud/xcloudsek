import { db } from "../../config";

const getUserHandler = async (req, res) => {
  const uuid = req.body.data;
  const user = await db.users.findUnique({
    where: {
      uuid: uuid,
    },
  });

  res.status(200).json({
    success: true,
    data: { user },
    message: "get the user",
  });
};

export default getUserHandler;

// export default function handler(req, res) {
//   const uuid = req.body.data;
//   const user = await
// }
