import { db } from "../../config";

const createUserHandler = async (req, res) => {
  const userData = req.body.data;
  userData["uuid"] = JSON.stringify(Date.now());
  const user = await db.users.create({
    data: userData,
  });
  res.status(200).json({
    success: true,
    data: { user },
    message: "Creating User is Successful",
  });
};

export default createUserHandler;
