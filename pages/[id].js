import SuccessMessage from "../components/SuccessMessage/SuccessMessage";
import SignedIn from "../components/SignedIn/SignedIn";
import { useRouter } from "next/router";
import { useGetUserData } from "../hooks/useGetUserData";
import Btn from "./../components/Btn/Btn";
import { Flex, Box } from "@chakra-ui/react";

const UUID = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data } = useGetUserData(id);
  return (
    <Flex
      w="100%"
      align="center"
      justify="space-between"
      direction="column"
      p={["0px 26px", "4px 60px"]}
    >
      <SuccessMessage username={data?.data.firstname} />
      <SignedIn />
      <Box p="30px 0px">
        <Btn buttonTitle="I want to Opt Out" />
      </Box>
    </Flex>
  );
};

export default UUID;
