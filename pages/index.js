import { Box } from "@chakra-ui/react";
import Form from "../components/Form/Form";
import Content from "../components/Content/Content";

export default function Home() {
  return (
    <Box p="4px 60px">
      <Content />
      <Form />
    </Box>
    // <div className={mainstyle.content_container}>
    //   <Content />
    //   <Form />
    // </div>
  );
}
