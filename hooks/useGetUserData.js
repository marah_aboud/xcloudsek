import { useQuery } from "react-query";
import axios from "axios";

const fetchUserData = (id) => {
  return axios.get(`http://localhost:4000/users/${id}`);
};
export const useGetUserData = (id) => {
  return useQuery(["user-data", id], () => fetchUserData(id));
};
