import { useMutation } from "react-query";
import axios from "axios";

const addUserData = (user) => {
  return axios.post("http://localhost:4000/users", user);
};

export const usePostUserData = () => {
  return useMutation(addUserData);
};
