import { Flex, Box } from "@chakra-ui/react";
import Image from "next/image";

const Header = () => {
  return (
    <Flex justify="right" p="40px 40px 22px 40px" w="100%">
      <Box w="139px" h="55px" position="relative">
        <Image src="/images/xvigil-logo.svg" alt="XVigil Logo" layout="fill" />
      </Box>
    </Flex>
  );
};

export default Header;
