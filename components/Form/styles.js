const styles = () => {
  return {
    headingText: {
      fontWeight: "600",
      fontSize: "16px",
      lineHeight: "24px",
      color: "#888888",
    },

    input: {
      width: "100%",
      border: "none",
      borderBottom: "1px solid #888888",
      padding: "8px",
      margin: "8px 0px",
      borderRadius: "0",
      fontSize: "14px",
    },
  };
};

export default styles;
