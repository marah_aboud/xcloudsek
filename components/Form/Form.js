import Btn from "../Btn/Btn";
import customStyles from "./styles";
import { Box, Heading, FormControl, Input, Center } from "@chakra-ui/react";
import { useState } from "react";
import { useRouter } from "next/router";
import { usePostUserData } from "../../hooks/usePostUserData";

const Form = () => {
  const styles = customStyles();
  const [email, setEmail] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [organization, setOrganization] = useState("");
  const [cms, setCsm] = useState("");
  const router = useRouter();

  const { mutate } = usePostUserData();

  const addUser = async () => {
    try {
      const userData = {
        email,
        firstname,
        lastname,
        organization,
        cms,
        id: Date.now(),
      };
      await mutate(userData);
      router.push(`/${parseInt(userData.id)}`);
    } catch (erro) {
      console.log(erro);
    }
  };

  // const addUser = async () => {
  //   const response = await axios.post("http://localhost:4000/users", {});
  //   const data = response.data;
  //   console.log(response.data);
  //   router.push(`/${parseInt(data.id)}`);
  // };
  return (
    <Box>
      <Heading as="h3" sx={styles.headingText} mb="7px" mt="65px">
        Sign Up for the waitlist of beta XVigilX
      </Heading>
      <FormControl pt="10px">
        <Input
          sx={styles.input}
          variant="unstyled"
          name="email"
          type="text"
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          sx={styles.input}
          variant="unstyled"
          name="firstname"
          placeholder="First Name"
          onChange={(e) => setFirstname(e.target.value)}
        />
        <Input
          sx={styles.input}
          variant="unstyled"
          name="lastname"
          placeholder="Last Name (Optional)"
          onChange={(e) => setLastname(e.target.value)}
        />
        <Input
          sx={styles.input}
          variant="unstyled"
          name="organization"
          placeholder="Organization"
          onChange={(e) => setOrganization(e.target.value)}
        />
        <Input
          sx={styles.input}
          variant="unstyled"
          name="csmanager"
          placeholder="Customer Success Manager (Optional)"
          onChange={(e) => setCsm(e.target.value)}
        />
      </FormControl>
      <Center p="56px 16px 30px">
        <Btn onClick={addUser} buttonTitle="Sign Up" />
      </Center>
    </Box>
  );
};

export default Form;
