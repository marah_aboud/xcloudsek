const styles = () => {
  return {
    btn: {
      backgroundColor: "#fff",
      border: "1px solid #dae7fb",
      borderRadius: "4px",
      boxShadow: "#00000026 0px 0px 4px",
      padding: "12px 60px",
      color: "#134ca7",
      cursor: "pointer",
      fontSize: "12px",
    },
  };
};

export default styles;
