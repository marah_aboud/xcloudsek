import customStyles from "./styles";
import { Button } from "@chakra-ui/react";

const Btn = (props) => {
  const styles = customStyles();
  return (
    // <div className={styles.container}>
    <Button
      sx={styles.btn}
      variant="unstyled"
      onClick={props.onClick}
      _hover={{ backgroundColor: "#134ca7", color: "#fff" }}
    >
      {props.buttonTitle}
    </Button>
    // </div>
  );
};

export default Btn;
