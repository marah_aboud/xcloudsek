// import { useTheme } from "@chakra-ui/react";
const styles = () => {
  // const them = useTheme();
  return {
    text: {
      fontWeight: "400",
      fontSize: "16px",
      lineHeight: "26px",
      color: "#888888",
    },
    blueText: {
      fontSize: "16px",
      lineHeight: "26px",
      fontWeight: "600",
      color: "#134CA7",
    },
    listText: {
      fontWeight: "400",
      color: "#134CA7",
      fontSize: "16px",
      lineHeight: "26px",
    },
  };
};

export default styles;
