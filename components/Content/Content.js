import customStyles from "./styles";
import { Box, UnorderedList, ListItem, Text } from "@chakra-ui/react";

const Content = () => {
  const styles = customStyles();
  return (
    <Box>
      <Text sx={styles.text} textAlign="center" mb="35px">
        Over the years, XVigil has expanded and evolved in scope and precision.
        And we owe the success of our product to the insights and feedback that
        we have consistently received from XVigil users like you.
      </Text>
      <Text sx={styles.blueText} mb="13px" pr="22px">
        XVigilX will enable you to structure and curate your digital risk feed
        as per your needs.
      </Text>
      <Box sx={styles.listText} pr="22px">
        <Text>
          With the new portal, you have the flexibility to decide how and when
          you:
        </Text>
        <UnorderedList m="0" pl="30px">
          <ListItem>Receive threat alerts and notifications</ListItem>
          <ListItem>
            Model and visualize your organization’s threat matrix
          </ListItem>
          <ListItem>Track and mitigate issues</ListItem>
        </UnorderedList>
      </Box>
    </Box>
  );
};

export default Content;
