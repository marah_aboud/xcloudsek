import customStyles from "./styles";
import { Box, Text, UnorderedList, ListItem } from "@chakra-ui/react";
const SignedIn = () => {
  const styles = customStyles();
  return (
    <Box>
      <Text sx={styles.text} mt="26px" textAlign={"center"}>
        Thank you for registering to be one of the first users of the new and
        advanced XVigil.{" "}
        <strong>XVigilX is the next phase of this journey.</strong>
      </Text>
      <Text sx={styles.text} mt="30px">
        Over the years, XVigil has expanded and evolved in scope and precision.
        And we owe the success of our product to the insights and feedback that
        we have consistently received from XVigil users like you.
      </Text>
      <Text sx={styles.text} mt="30px">
        XVigilX will enable you to structure and curate your digital risk feed
        as per your needs.
      </Text>
      <Box sx={styles.text} mt="20px">
        <Text fontWeight="600">
          With the new portal, you have the flexibility to decide how and when
          you:
        </Text>
        <UnorderedList className={styles.ul} fontWeight={"600"} m="0" pl="22px">
          <ListItem>Receive threat alerts and notifications</ListItem>
          <ListItem>
            Model and visualize your organization’s threat matrix
          </ListItem>
          <ListItem>Track and mitigate issues</ListItem>
        </UnorderedList>
      </Box>

      <Text sx={styles.text} mt="30px">
        Note: You’re on this page because you scanned a QR code that allows you
        to sign-up for the beta version of XVigilX, which will go-live at the
        end of March 2022. If you wish to opt-out of this program, please click
        on the button below, or reach out to us at{" "}
        <a href="care@cloudsek.com">care@cloudsek.com</a>.
      </Text>
    </Box>
  );
};

export default SignedIn;
