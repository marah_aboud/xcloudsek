const styles = () => {
  // const them = useTheme();
  return {
    text: {
      fontWeight: "400",
      lineHeight: "20px",
      fontSize: "12px",
      color: "#888888",
      // textAlign: "center",
    },
    blueText: {
      fontSize: "16px",
      lineHeight: "26px",
      fontWeight: "600",
      color: "#134CA7",
    },
    listText: {
      fontWeight: "400",
      color: "#134CA7",
      fontSize: "16px",
      lineHeight: "26px",
    },
  };
};

export default styles;
