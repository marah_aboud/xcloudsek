import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { Box, Flex } from "@chakra-ui/react";
import customStyles from "./styles";

const Layout = ({ children }) => {
  const styles = customStyles();
  return (
    <Flex sx={styles.outerContainer} justify="center">
      <Flex
        sx={styles.innerContainer}
        justify="space-between"
        direction="column"
        align="center"
      >
        <Header />
        <main>{children}</main>
        <Footer />
      </Flex>
    </Flex>
  );
};

export default Layout;
