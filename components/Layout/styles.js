const styles = () => {
  return {
    outerContainer: {
      backgroundColor: "#134ca7",
      width: "100%",
      minHeight: "100vh",
    },
    innerContainer: {
      width: "626px",
      minHight: "100vh",
      backgroundColor: "#fff",
    },
  };
};

export default styles;
