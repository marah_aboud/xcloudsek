const styles = () => {
  return {
    footer: {
      background: "#edf3fd",
      padding: "17px",
      width: "100%",
      color: "#134ca7",
    },
    link: {
      display: "inline",
    },
  };
};

export default styles;
