import customStyles from "./styles";

import { Text, Box, Flex, List, ListItem } from "@chakra-ui/react";

const Footer = () => {
  const styles = customStyles();
  return (
    <Flex
      sx={styles.footer}
      justify="center"
      align="center"
      direction="column"
      fontSize={["10px", "12px"]}
    >
      <Text>&copy;2022, XVigil, All rights reserved</Text>
      <List pl="0">
        <ListItem sx={styles.link} p={["0px 16px", "0px 50px"]}>
          <a href="https://cloudsek.com/privacy/">Privacy policy</a>
        </ListItem>
        <ListItem sx={styles.link} p={["0px 16px", "0px 50px"]}>
          <a href="https://xvigil.com/terms">Terms of Service</a>
        </ListItem>
      </List>
    </Flex>
  );
};

export default Footer;
