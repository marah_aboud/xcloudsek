import { Box, Heading } from "@chakra-ui/react";
import customStyles from "./styles";

const SuccessMessage = (props) => {
  const styles = customStyles();
  return (
    <Box sx={styles.messageContainer}>
      <Box sx={styles.headingH3}>
        Congratulations {props.username}!
        <Heading as="h4" sx={styles.headingH4}>
          You’re now on the waitlist for XVigilX - The future of XVigil!
        </Heading>
      </Box>
    </Box>
  );
};

export default SuccessMessage;
