const styles = () => {
  return {
    messageContainer: {
      padding: "0 72px",
      background: "url(/images/mail-img.svg) no-repeat center",
      textAlign: "center",
    },
    headingH3: {
      padding: "283px 62px 8px",
      margin: "0",
      fontSize: "18px",
      fontWeight: "600",
      color: "#414141",
    },
    headingH4: {
      margin: "6px 0px 0px",
      fontSize: "16px",
      fontWeight: "400",
      lineHeight: "22px",
      color: "#414141",
    },
  };
};

export default styles;
